#ifdef GL_ES
//浮点精度
precision mediump float;
#endif

#define PI 3.14159265359
#define TWO_PI 6.28318530718

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

#define TRANSLATE
// #define ROTATE
// #define SCALE

float circle(vec2 st,vec2 center,float radius)
{
    return 1.-step(radius,length(st-center));
}

//多边形
vec3 polygon(vec2 st,vec2 center,float edge_num,float size)
{
    vec2 pos=st-center;
    float a=atan(pos.y,pos.x)+PI/2.;
    float r=TWO_PI/edge_num;
    // 这里有点不太明白
    float d=cos(floor(.5+a/r)*r-a)*length(pos);
    return vec3(1.-step(size,d));
}

mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
    sin(_angle),cos(_angle));
}

mat2 scale(vec2 _scale)
{
    return mat2(_scale.x,0.,
    0.,_scale.y);
}

#ifdef TRANSLATE
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    vec3 color=vec3(0.);
    vec2 translate=vec2(cos(u_time),sin(u_time));
    //给 st 变量加上一个包含每个片段的位置的向量
    st+=translate*-.3;
    color=vec3(st.x,st.y,0.);
    color+=vec3(circle(st,vec2(.5,.5),.1));
    gl_FragColor=vec4(color,1.);
}
#endif

#ifdef ROTATE
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    vec3 color=vec3(0.);
    
    //先平移回原点
    st-=vec2(.5);
    //再旋转
    st=rotate2d(sin(u_time)*PI)*st;
    //再平移回去
    st+=vec2(.5);
    
    color=vec3(st.x,st.y,0.);
    
    color+=polygon(st,vec2(.5,.5),3.,.1);
    
    gl_FragColor=vec4(color,1.);
}
#endif

#ifdef SCALE
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    vec3 color=vec3(0.);
    
    //先平移到原点
    st-=vec2(.5);
    //缩放
    st=scale(vec2(sin(u_time)+1.))*st;
    //再平移回去
    st+=vec2(.5);
    
    color=vec3(st.x,st.y,0.);
    
    color+=polygon(st,vec2(.5,.5),3.,.1);
    
    gl_FragColor=vec4(color,1.);
}
#endif