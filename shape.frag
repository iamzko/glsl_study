#ifdef GL_ES
//浮点精度
precision mediump float;
#endif

// #define RECT
#define RECT_FUNC

#define PI 3.14159265359
#define TWO_PI 6.28318530718

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

//方格纸上的每个小方形格点就是一个线程（一个像素）。
#ifdef RECT
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    vec3 color=vec3(0.);
    
    #if 0
    float left=step(.1,st.x);
    float bottom=step(.1,st.y);
    float right=1.-step(.9,st.x);
    //也可以写作
    right=step(.1,1.-st.x);
    float top=1.-step(.9,st.y);
    top=step(.1,1.-st.y);
    
    //left 乘 bottom 效果相当于逻辑 AND —— 当 x y 都为 1.0 时乘积才能是 1.0。
    // 相乘类似与操作
    color=vec3(left*bottom*right*top);
    #else
    //直接按向量计算
    vec2 lb=step(vec2(.1),st);
    vec2 rt=step(vec2(.1),1.-st);
    float rect=lb.x*lb.y*rt.x*rt.y;
    color=vec3(rect);
    #endif
    
    gl_FragColor=vec4(color,1.);
}
#endif

#ifdef RECT_FUNC
float rect(vec2 st,vec2 center,vec2 size)
{
    //st.x 小于center.x，所以第一个参数为负数
    float left=step(-size.x/2.,st.x-center.x);
    float bottom=step(-size.y/2.,st.y-center.y);
    float right=step(-size.x/2.,1.-st.x-center.x);
    float top=step(-size.y/2.,1.-st.y-center.y);
    return left*bottom*right*top;
}

float rect(vec2 pt,vec2 anchor,vec2 size,vec2 center){
    vec2 p=pt-center;
    vec2 halfsize=size*.5;
    float horz=step(-halfsize.x-anchor.x,p.x)-step(halfsize.x-anchor.x,p.x);
    float vert=step(-halfsize.y-anchor.y,p.y)-step(halfsize.y-anchor.y,p.y);
    return horz*vert;
}
float circle(vec2 st,vec2 center,float radius)
{
    return 1.-step(radius,length(st-center));
}
float circle(in vec2 _st,in float _radius){
    vec2 dist=_st-vec2(.5);
    return 1.-smoothstep(_radius-(_radius*.01),
    _radius+(_radius*.01),
    dot(dist,dist)*4.);
}
//距离场
float distance(vec2 st,vec2 center,float radius)
{
    return fract(length(st-center)*radius);
}
//极坐标下三角函数
vec3 polar_cos(vec2 st,vec2 center,float radius,float num)
{
    vec2 pos=st-center;
    //极坐标转换
    float r=length(pos)*radius;
    float a=atan(pos.y,pos.x);
    float f=cos(a*num);
    return vec3(1.-step(f,r));
}
//多边形
vec3 polygon(vec2 st,vec2 center,float edge_num,float size)
{
    vec2 pos=st-center;
    float a=atan(pos.y,pos.x)+PI/2.;
    float r=TWO_PI/edge_num;
    // 这里有点不太明白
    float d=cos(floor(.5+a/r)*r-a)*length(pos);
    return vec3(1.-step(size,d));
}

void main(){
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    vec3 color=vec3(0.);
    color=vec3(rect(st,vec2(.5,.5),vec2(.9,.9)));
    // color = vec3(1.0-rect(st,vec2(0.,0.),vec2(0.6,0.8),vec2(0.5,0.5))+ rect(st,vec2(0.,0.),vec2(0.5,0.7),vec2(0.5,0.5)));
    // color=vec3(circle(st,vec2(.5,.5),.5));
    // color=vec3(distance(st,vec2(.5,.5),10.));
    // color=polar_cos(st,vec2(.5,.5),3.,4.);
    // color=polygon(st,vec2(.5,.5),3.,.1);
    gl_FragColor=vec4(color,1.);
}
#endif