#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

void main() {
    //上述代码中我们用 gl_FragCoord.xy 除以 u_resolution，对坐标进行了规范化。这样做是为了使所有的值落在 0.0 到 1.0 之间，这样就可以轻松把 X 或 Y 的值映射到红色或者绿色通道。
	vec2 st = gl_FragCoord.xy/u_resolution;
    vec2 mouse_st = u_mouse.xy/u_resolution;
	gl_FragColor = vec4(st.x,st.y,0.0,1.0);
//    gl_FragColor = vec4(mouse_st.x,mouse_st.y,0.0,1.0);
}
//就像 GLSL 有个默认输出值 vec4 gl_FragColor 一样，它也有一个默认输入值（ vec4 gl_FragCoord ）。gl_FragCoord存储了活动线程正在处理的像素或屏幕碎片的坐标。有了它我们就知道了屏幕上的哪一个线程正在运转。为什么我们不叫 gl_FragCoord uniform （统一值）呢？因为每个像素的坐标都不同，所以我们把它叫做 varying（变化值）。