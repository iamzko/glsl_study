#ifdef GL_ES
precision mediump float;//中等精度的浮点
#endif

#define CIRCLE
// #define BALL
// #define PLOT


#define PI 3.141592653589793

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

#ifdef CIRCLE
//第一个例子，画圆
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution-.5;
    vec3 color=vec3(1,0,0);
    // gl_FragColor = vec4(color,1);
    float length1=length(st);
    float step1=1.-step(.5,length1);
    
    gl_FragColor=vec4(step1,step1,step1,1.);
}
#endif
#ifdef BALL
//第二个例子，带阴影的球
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution-.5;
    vec3 color=vec3(1);
    float length1=length(st);
    
    float sphere=max(0.,length1*2.);
    float sphereA=step(.5,sphere);
    //画圆
    color*=clamp(sphereA,0.,.75);
    //高光
    color+=(1.-length(st-vec2(-.12,.12))*3.)*(1.-sphereA);
    
    float refLight=1.-sphereA;
    refLight*=smoothstep(.3,.5,(length(st*.5+vec2(.05,-0.)))*2.);
    refLight=clamp(refLight,0.,1.)*.75;
    color+=refLight;
    //阴影
    float sha=smoothstep(.5,.65,length(st*vec2(.2,1.)+vec2(-.05,.22))*8.);
    sha+=(1.-smoothstep(.5,.65,length(st*vec2(.2,1.)+vec2(-.05,.22))*8.))*.5;
    sha=clamp(sha+1.-sphereA,0.,1.);
    color*=sha;
    gl_FragColor=vec4(color,1.);
}
#endif
#ifdef PLOT
//第三个例子，学习常用的函数
float plot(vec2 st)
{
    //当给定一个范围的上下限和一个数值，这个函数会在已有的范围内给出插值。前两个参数规定转换的开始和结束点，第三个是给出一个值用来插值。
    // smoothstep (x y a )参数   y必须大于x 然后 a如果小于x 返回 0 如果a>y 返回1 在x y之间 返回  3a^2-2a^3
    return smoothstep(0.0,0.02,abs(st.y-st.x));
}
float plot(vec2 st,float pct){
    return smoothstep(pct-.02,pct,st.y)-
    smoothstep(pct,pct+.02,st.y);
}
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution;
    // float y = smoothstep(0.1,0.9,st.x);
    float y = smoothstep(0.2,0.5,st.x) - smoothstep(0.5,0.8,st.x);;
    // vec3 color=vec3(st.y);
    vec3 color=vec3(y);
    float pct=plot(st,y);
    pct = plot(st);
    color=(1.-pct)*color+pct*vec3(1.,0.,0.);
    color = vec3(1.0 - pct)*color + pct * vec3(1.0,0.0,0);
    // color = vec3(smoothstep(0.,0.3,st.x-st.y));
    color = vec3(pct);
    gl_FragColor=vec4(color,1.);
    
}
//常用的函数
//step() 插值函数需要输入两个参数。第一个是极限或阈值，第二个是我们想要检测或通过的值。对任何小于阈值的值，返回 0.0，大于阈值，则返回 1.0。
//三角函数 sin cos等
//绝对值 abs
//取小数 fract
//向正无穷取整 ceil
//向负无穷取整 floor
//取模 mod
//提取正负号 sign
//将值约束为位于其他两个值之间 clamp(n,edge0,edge1)  edge0 <= edge1
// min max
#endif