#ifdef GL_ES
//浮点精度
precision mediump float;
#endif

#define PI 3.14159265359
#define TWO_PI 6.28318530718

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

// #define TILE
#define BRICK

vec2 brickTile(vec2 _st, float _zoom)
{
    _st *= _zoom;

    //y为偶数时不偏移，为奇数时偏移
    _st.x += step(1., mod(_st.y,2.0)) * 0.5;

    return fract(_st);
}

float rect(vec2 st,vec2 center,vec2 size)
{
    //st.x 小于center.x，所以第一个参数为负数
    float left=step(-size.x/2.,st.x-center.x);
    float bottom=step(-size.y/2.,st.y-center.y);
    float right=step(-size.x/2.,1.-st.x-center.x);
    float top=step(-size.y/2.,1.-st.y-center.y);
    return left*bottom*right*top;
}

float circle(vec2 st,vec2 center,float radius)
{
    return 1.-step(radius,length(st-center));
}

vec2 tile (vec2 _st, float _zoom) 
{
    _st *= _zoom;
    return fract(_st);
}

vec2 rotate2D (vec2 _st, float _angle) 
{
    _st -= 0.5;
    _st =  mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle)) * _st;
    _st += 0.5;
    return _st;
}

vec2 rotateTilePattern(vec2 _st)
{

    // 细分成四个区域
    _st *= 2.0;

    // 确定区域位置
    float index = 0.0;
    index += step(1., mod(_st.x,2.0));
    index += step(1., mod(_st.y,2.0))*2.0;

    //      |
    //  2   |   3
    //      |
    //--------------
    //      |
    //  0   |   1
    //      |

    // Make each cell between 0.0 - 1.0
    _st = fract(_st);

    // Rotate each cell according to the index
    if(index == 1.0){
        //  Rotate cell 1 by 90 degrees
        _st = rotate2D(_st,PI*0.5);
    } else if(index == 2.0){
        //  Rotate cell 2 by -90 degrees
        _st = rotate2D(_st,PI*-0.5);
    } else if(index == 3.0){
        //  Rotate cell 3 by 180 degrees
        _st = rotate2D(_st,PI);
    }

    return _st;
}

// fract() 函数。它返回一个数的分数部分，本质上是除1的余数（mod(x,1.0)）。换句话说， fract() 返回小数点后的数。
#ifdef BRICK
void main()
{
    vec2 st = gl_FragCoord.xy/u_resolution;
    st *= 3.;
    // st += vec2(cos(u_time),sin(u_time))*0.4;
    st = brickTile(st,5.0);
    vec3 color = vec3(0.0);
    st = fract(st);
    color = vec3(circle(st,vec2(.5,.5),.1))*vec3(1.0,0.0,0.0);
    color = vec3(rect(st,vec2(0.5,0.5),vec2(0.9,0.8)));
    gl_FragColor = vec4(color,1.0);
}
#endif

#ifdef TILE
void main()
{
    vec2 st = gl_FragCoord.xy/u_resolution;
    // st = tile(st,3.0);
    st = rotateTilePattern(st);
    // st += vec2(cos(u_time),sin(u_time))*0.4;
    st = rotate2D(st,u_time);
    vec3 color = vec3(0.0);
    color = vec3(step(st.x,st.y));
    gl_FragColor = vec4(color,1.0);
}
#endif