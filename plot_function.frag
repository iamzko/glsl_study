#ifdef GL_ES
//浮点精度
precision mediump float;
#endif

#define PI 3.14159265359

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

float plot(vec2 st,float pct)
{
    //相减之后函数值的区域留白，其他地方都是黑的
    return smoothstep(pct-.02,pct,st.y)-
    smoothstep(pct,pct+.02,st.y);
}
float step_plot(vec2 st,float pct)
{
    return step(pct-0.02,st.y) - step(pct+0.02,st.y);
}

void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution;
    st *= 6.;
    st += -3.0;

    //图像函数
    //指数函数
    float y=pow(st.x,5.);
    // y = sqrt(st.x);
    //直线函数
    // y=st.x;
    //正弦函数
    // y=.5*sin(st.x*PI*2.)+.5;
    //余弦函数
    // y=.5*cos(st.x*PI*2.)+.5;
    //对数函数
    // y=log(st.x*10.);
    //step 第一个值是阈值，第二个值是进行检查的值，任何小于阈值的值，返回0.0，大于阈值，返回1.0
    // y = step(0.5,st.x);
    //snoothstep使用前两个值去检测第三个值，如果被检测值小于第一个值，返回0.0，大于第二个值，返回1.0，在两值中间，返回3.*x^2 + 2.0* x^3
    // y = smoothstep(0.1,0.9,st.x);
    //clamp 第一个值为检测值，后两个值为下限和上限，在上下限之内则返回第一个值，小于下限，返回下限，大于上限，返回上限
    // y = clamp(st.x,0.1,0.9);
    // 返回 x 对 0.5 取模的值
    // y = mod(st.x,0.5); 
    // 仅仅返回数的小数部分
    // y = fract(st.x); 
    // 向正无穷取整
    // y = ceil(st.x);  
    // 向负无穷取整
    // y = floor(st.x); 
    // 提取 x 的正负号
    // y = sign(st.x);  
    // 返回 x 的绝对值
    // y = abs(st.x);   
    // 返回 x 和 0.0 中的较小值
    // y = min(0.0,st.x);
    // 返回 x 和 0.0 中的较大值     
    // y = max(0.0,st.x);   
    vec3 color=vec3(y);
    float pct=plot(st,y);
    pct = step_plot(st,y);
    color=(1.-pct)*color+pct*vec3(0.,1.,0.);
    
    gl_FragColor=vec4(color,1.);
}
