#ifdef GL_ES
//浮点精度
precision mediump float;
#endif

#define PI 3.14159265359
#define TWO_PI 6.28318530718

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;


vec3 rgb2hsb(in vec3 c)
{
    vec4 K=vec4(0.,-1./3.,2./3.,-1.);
    vec4 p=mix(vec4(c.bg,K.wz),
    vec4(c.gb,K.xy),
    step(c.b,c.g));
    vec4 q=mix(vec4(p.xyw,c.r),
    vec4(c.r,p.yzx),
    step(p.x,c.r));
    float d=q.x-min(q.w,q.y);
    float e=1.e-10;
    return vec3(abs(q.z+(q.w-q.y)/(6.*d+e)),d/(q.x+e),q.x);
}

//  Function from Iñigo Quiles
//  https://www.shadertoy.com/view/MsS3Wc
vec3 hsb2rgb(in vec3 c)
{
    vec3 rgb=clamp(abs(mod(c.x*6.+vec3(0.,4.,2.),6.)-3.)-1.,0.,1.);
    rgb=rgb*rgb*(3.-2.*rgb);
    return c.z*mix(vec3(1.),rgb,c.y);
}

#if 1
//笛卡尔映射
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution;
    vec3 color=vec3(0.);
    // 色彩映射到x上
    // 亮度映射到y上
    color=hsb2rgb(vec3(st.x,1.,st.y));
    gl_FragColor=vec4(color,1.);
}
#else
//极坐标映射
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution;
    vec3 color=vec3(0.);

    //空间翻转且平移
    vec2 toCenter=vec2(.5)-st;
    //将x y 转换为角度和半径
    float angle=atan(toCenter.y,toCenter.x);
    float radius=length(toCenter)*2.;

    //将hsb颜色映射到极坐标上
    //将颜色映射到角度上，饱和度映射到半径上
    color=hsb2rgb(vec3((angle/TWO_PI)+.5,radius,1.))*(1.-step(.5,length(toCenter)))+step(.5,length(toCenter));

    gl_FragColor=vec4(color,1.);
}
#endif