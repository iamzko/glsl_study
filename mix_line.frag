#ifdef GL_ES
//浮点精度
precision mediump float;
#endif

#define MIX_LINE
// #define FLAG

#define PI 3.14159265359

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

vec3 colorA=vec3(.149,.141,.912);
vec3 colorB=vec3(1.,.833,.224);

float plot(vec2 st,float pct)
{
    return smoothstep(pct-.01,pct,st.y)-
    smoothstep(pct,pct+.01,st.y);
}
#ifdef MIX_LINE
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    vec3 color=vec3(0.);
    
    vec3 pct=vec3(st.x);
    
    pct.r=smoothstep(0.,1.,st.x);
    pct.g=sin(st.x*PI);
    pct.b=pow(st.x,.5);
    //背景的颜色
    color=mix(colorA,colorB,pct);
    
    #if 0
    //plot 返回的函数图形所在是1.0，其他地方是0.0，mix之后只有函数图形的地方被上色了
    color=mix(color,vec3(1.,0.,0.),plot(st,pct.r));
    color=mix(color,vec3(0.,1.,0.),plot(st,pct.g));
    color=mix(color,vec3(0.,0.,1.),plot(st,pct.b));
    #else
    //也可以写作
    color=(1.-(plot(st,pct.r)+plot(st,pct.g)+plot(st,pct.b)))*color
    +plot(st,pct.r)*vec3(1.,0.,0.)
    +plot(st,pct.g)*vec3(0.,1.,0.)
    +plot(st,pct.b)*vec3(0.,0.,1.);
    #endif
    
    gl_FragColor=vec4(color,1.);
}
#endif
#ifdef FLAG
//五色旗的例子
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    vec3 color=vec3(0.);
    color=vec3(1.,0.,0.)*(1.-step(.2,st.x))
    +vec3(1.,1.,0.)*(step(.2,st.x)-step(.4,st.x))
    +vec3(0.,0.,1.)*(step(.4,st.x)-step(.6,st.x))
    +vec3(0.0, 1.0, 0.0824)*(step(.6,st.x)-step(.8,st.x))
    +vec3(0.0275, 0.9569, 0.9882)*(step(.8,st.x)-step(1.0,st.x))
    ;
    gl_FragColor=vec4(color,1.);
    
}
#endif