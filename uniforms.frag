#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution; // 画布尺寸（宽，高）
uniform vec2 u_mouse;      // 鼠标位置（在屏幕上哪个像素）
uniform float u_time;     // 时间（加载后的秒数）

void main() 
{
    //动态变化的红色
	gl_FragColor = vec4(abs(sin(u_time)),0.0,0.0,1.0);
}
//GPU 的硬件加速支持我们使用角度，三角函数和指数函数。这里有一些这些函数的介绍：sin(), cos(), tan(), asin(), acos(), atan(), pow(), exp(), log(), sqrt(), abs(), sign(), floor(), ceil(), fract(), mod(), min(), max() 和 clamp()。