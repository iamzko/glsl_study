# **什么是GLSL？**

* GLSL是OpenGL **Shader** Language的缩写
* GLSL语法类似C语言
* GLSL是在OpenGL 2.0引入的
* GLSL通常用来编写Shader

---

# **什么是Shader？**

* Shader是一段GLSL（openGL着色语言）小程序，运行在GPU(图形处理器)，而非CPU
* 通过以下5步使用一个Shader：
  * 创建着色器（顶点和片元）
  * 编译着色器
  * 把着色器添加到程序中
  * 链接程序
  * 使用程序

![1671972618324](image/GLSL入门/1671972618324.png)

---

## **创建着色器**

```cpp
GLuint glCreateShader（GLenum shaderType）;
```

shaderType为 `GL_VERTEX_SHADER`或者是 `GL_FRAGMENT_SHADER`，如下：

```cpp
unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
```

我们把需要创建的**着色器类型以参数形式**提供给 `glCreateShader`。由于我们正在创建一个顶点着色器，传递的参数是 `GL_VERTEX_SHADER`。

下一步我们把这个着色器源码附加到着色器对象上：

```cpp
void  glShaderSource（GLuint  shader ，GLsizei  count ，const GLchar * const * string ，const GLint * length ）;
```

顶点着色器源码（储存在**一个**C的字符串中），所以第二个参数为1，第三个参数是顶点着色器真正的源码。

```
glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
```

顶点着色器源码类似如下形式：

```c
#version 330 core
layout(location = 0) in vec3 vert;
void main() {
    gl_Position = vec4(vert, 1.0);//或者是gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);
}
```

片元着色器源码类似如下形式：

```c
#version 330 core 
out vec4 FragColor;
void main() { 
　　FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);
} 
```

---

## 编译着色器

使用 `glCompileShader(Gluint shader)`编译着色器。结果查询使用 `glGetShaderiv(Gluint shader,GL_COMPILE_STATUS,&compiled)`查询：

```cpp
int  success;
char infoLog[512];
glCompileShader(vertexShader);
glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
```

首先我们定义了一个整型变量来表示是否成功编译，还定义了一个存储错误消息的容器（如果有的话）。然后用 `glGetShaderiv`检查是否编译成功。如果编译失败，用 `glGetShaderInfoLog`获取错误消息，然后打印出来。

```cpp
if(!success)
{
    glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
    std::cout << "顶点着色器编译错误\n" << infoLog << std::endl;
}
```

---

## 把着色器添加到程序中

```cpp
GLuint glCreatPogram();
```

创建一个空的程序

```cpp
void glAttachShader(GLuint program,GLuint shader);
```

把着色器加到程序中

---

## 链接程序

链接程序的时候会把顶点着色器的输出作为片段着色器的输入。

```cpp
void glLinkProgram(GLuint program);
```

---

## 使用程序

```cpp
void glUseProgram(GLuint program);
```

---

# GLSL**变量‍**

GLSL的变量名称可以使用字母，数字以及下划线，不能以数字开头， gl_作为GLSL保留前缀只能用于内部变量。还有一些GLSL内置函数名称是不能够作为变量的名称。

## 数据类型

### 基础数据类型

| 类型            | 描述                                                                                          |
| --------------- | --------------------------------------------------------------------------------------------- |
| **void**  | 跟**C**语言的**void**类似，表示空类型。作为函数的返回类型，表示这个函数不返回值。 |
| **bool**  | **布尔类型** ，**true**或 **false** ，以及可以产生布尔型的表达式。          |
| **int**   | **有符号整型**                                                                          |
| **uint**  | **无符号整形**                                                                          |
| **float** | **浮点型**                                                                              |

### 纹理采样类型

| 类型                      | 描述                                                                                      |
| ------------------------- | ----------------------------------------------------------------------------------------- |
| **sampler1D**       | 用于内建的纹理函数中引用指定的**1D纹理的句柄** 。只可以作为一致变量或者函数参数使用 |
| **sampler2D**       | **二维纹理句柄**                                                                    |
| **sampler3D**       | **三维纹理句柄**                                                                    |
| **samplerCube**     | **cube map 纹理句柄**                                                               |
| **sampler1DShadow** | **一维深度纹理句柄**                                                                |
| **sampler2DShadow** | **二维深度纹理句柄**                                                                |

### 聚合类型（向量和矩阵类型）

* **向量类型**

| 类型              | 描述                              |
| ----------------- | --------------------------------- |
| vec2,vec3,vec4    | 2分量、3分量和4分量浮点向量       |
| ivec2,ivec3,ivec4 | 2分量、3分量和4分量整数向量       |
| uvec2,uvec3,uvec4 | 2分量、3分量和4分量无符号整数向量 |
| bvec2,vbec3,bvec4 | 2分量、3分量和4分量布尔向量       |

**A、向量声明--4分量的float 类型向量**

```none
vec4 V1;
```

**B、声明向量并对其进行构造**

```none
vec4 V2 = vec4(1,2,3,4);
```

**C、向量运算**

```c
vec4 v;
vec4 vOldPos = vec4(1,2,3,4);
vec4 vOffset = vec4(1,2,3,4);
//注意：接下来假设所有参与运算的变量已定义并赋值。
v = vOldPos + vOffset;
v = vNewPos;
v += vec4(10,10,10,10);
v = vOldPos * vOffset;
v *= 5;
```

**D、向量元素的获取（成分选择）**

向量中单独的成分可以通过 **{x,y,z,w}, {r,g,b,a}** 或者 **{s,t,p,q}** 的记法来表示。这些不同的记法用于  **顶点** ， **颜色** ， **纹理坐标** 。在成分选择中，你不可以混合使用这些记法。其中 **{s,t,p,q}** 中的 **p** 替换了**纹理**的 **r** 坐标，因为与颜色 **r** 重复了。下面是用法举例： 例如有向量 **v1** 和  **v2** :

```c
vec3 v1 = {0.5, 0.35, 0.7};
vec4 v2 = {0.1, 0.2, 0.3, 0.4};
```

可以通过 **{x,y,z,w}, {r,g,b,a}** 或者 **{s,t,p,q}** 来取出向量中的元素值。 通过  **x,y,z,w** ：

```c
v2.x = 3.0f;
v2.xy = vec2(3.0f,4.0f);
v2.xyz = vec3(3,0f,4,0f,5.0f);
```

通过  **r,g,b,a** ：

```c
v2.r = 3.0f;
v2.rgba = vec4(1.0f,1.0f,1.0f,1.0f);
```

通过  **s,t,q,r** ：

```none
v2.stqr = vec2(1.0f, 0.0f, 0.0f, 1.0f);
```

错误示例：

```none
float myQ = v1.q;// 出错，数组越界访问，q代表第四个元素
float myRY = v1.ry; // 不合法，混合使用记法
```

向量还支持一次性对所有分量操作

```none
v1.x = v2.x +5.0f; 
v1.y = v2.y +4.0f; 
v1.z = v2.z +3.0f;
v1.xyz = v2.xyz + vec3(5.0f,4.0f,3.0f);
```

* **矩阵类型**

| 类型           | 描述                                         |
| -------------- | -------------------------------------------- |
| mat2 或 mat2x2 | 2x2的浮点数矩阵类型                          |
| mat3 或 mat3x3 | 3x3的浮点数矩阵类型                          |
| mat4 或 mat4x4 | 4x4的浮点数矩阵类型                          |
| mat2x3         | 2列3行的浮点矩阵（OpenGL的矩阵是列主顺序的） |
| mat2x4         | 2列4行的浮点矩阵                             |
| mat3x2         | 3列2行的浮点矩阵                             |
| mat3x4         | 3列4行的浮点矩阵                             |
| mat4x2         | 4列2行的浮点矩阵                             |
| mat4x3         | 4列3行的浮点矩阵                             |

**创建矩阵：**

```none
mat4 m1,m2,m3;
```

**构造单元矩阵：**

```none
mat4 m2 = mat4(1.0f,0.0f,0.0f,0.0f
                    0.0f,1.0f,0.0f,0.0f,
                    0.0f,0.0f,1.0f,0.0f,
                    0.0f,0.0f,0.0f,1.0f);
```

**或者**

```none
mat4 m4 = mat4(1.0f);
```

### 向量矩阵运算

1、不同类型 float 与 int 间的运算:**

**float** 与 **int** 之间进行运算，需要进行一次显示转换，以下表达式都是正确的:

```c
int a = int(2.0);
float a = float(2);

int a = int(2.0)*2 + 1;
float a = float(2)*6.0+2.3;
```

---

**2、float 与 vec(向量)、mat(矩阵) 的运算:**

> * **逐分量运算** **vec，mat** 这些类型其实是由 **float** 复合而成的，当它们与**float** 运算时，其实就是在每一个分量上分别与 **float** 进行运算，这就是所谓的  **逐分量运算** 。**GLSL** 里，大部分涉及 **vec，mat** 的运算都是逐分量运算，但也并不全是。下文中就会讲到特例。 **逐分量运算** 是线性的，这就是说 vec 与 float 的运算结果是还是  **vec** 。

**int** 与 **vec，mat** 之间是不可运算的，因为 **vec** 和 **mat** 中的每一个分量都是 **float** 类型的，无法与 **int** 进行逐分量计算。

下面枚举了几种 **float** 与 **vec，mat** 运算的情况：

```c
vec3 a  = vec3(1.0, 2.0, 3.0);
mat3 m  = mat3(1.0);
float s = 10.0;

vec3 b  = s * a; // vec3(10.0, 20.0, 30.0)
vec3 c  = a * s; // vec3(10.0, 20.0, 30.0)
mat3 m2 = s * m; // = mat3(10.0)
mat3 m3 = m * s; // = mat3(10.0)
```

**3、vec(向量) 与 vec(向量)运算：**

两向量间的运算首先要保证操作数的阶数都相同，否则不能计算。例如: **`vec3\*vec2`** 和 **`vec4+vec3`** 等等都是不行的。

它们的计算方式是两操作数在同位置上的分量分别进行运算，其本质还是逐分量进行的，这和上面所说的 **float** 类型的逐分量运算可能有一点点差异，相同的是 **vec** 与 **vec** 运算结果还是  **vec** ，且阶数不变。

```c
vec3 a = vec3(1.0, 2.0, 3.0);
vec3 b = vec3(0.1, 0.2, 0.3);
vec3 c = a + b; // = vec3(1.1, 2.2, 3.3);
vec3 d = a * b; // = vec3(0.1, 0.4, 0.9);
```

AB=[a1,1a1,2a2,1a2,2][b1,1b1,2b2,1b2,2]=[a1,1b1,1+a1,2b2,1a1,1b1,2+a1,2b2,2a2,1b1,1+a2,2b2,1a2,1b1,2+a2,2b2,2]

**4、vec(向量) 与 mat(矩阵):**

要保证操作数的阶数相同，且 **vec** 与 **mat** 间只存在乘法运算。 它们的计算方式和线性代数中的矩阵乘法相同， **不是逐分量运算** 。

```c
vec2 v = vec2(10., 20.);
mat2 m = mat2(1., 2.,  3., 4.);
vec2 w = m * v; // = vec2(1. * 10. + 3. * 20., 2. * 10. + 4. * 20.)

vec2 v = vec2(10., 20.);
mat2 m = mat2(1., 2.,  3., 4.);
vec2 w = v * m; // = vec2(1. * 10. + 2. * 20., 3. * 10. + 4. * 20.)
```

**5、mat(矩阵) 与 mat(矩阵):**

要保证操作数的阶数相同。

在 **mat** 与 **mat** 的运算中，除了乘法是线性代数中的矩阵乘法外，其余的运算仍为 **逐分量运算** 。简单说就是只有乘法是特殊的，其余都和 **vec** 与 **vec** 运算类似。

```c
mat2 a = mat2(1., 2.,  3., 4.);
mat2 b = mat2(10., 20.,  30., 40.);
mat2 c = a * b; // mat2(1.*10.+3.*20.,2.*10.+4.*20.,1.* 30.+3.*40.,2.* 30.+4.*40.);

mat2 d = a+b;// mat2(1.+10.,2.+20.,3.+30.,4.+40);
```

### 类型转换

**glsl可以使用构造函数进行显式类型转换,各值如下:**

```c
bool t= true;
bool f = false;

int a = int(t); //true转换为1或1.0
int a1 = int(f);//false转换为0或0.0

float b = float(t);
float b1 = float(f);

bool c = bool(0);//0或0.0转换为false
bool c1 = bool(1);//非0转换为true

bool d = bool(0.0);
bool d1 = bool(1.0);
```

**glsl中,没有隐式类型转换,原则上glsl要求任何表达式左右两侧(l-value),(r-value)的类型必须一致 也就是说以下表达式都是错误的:**

```c
int a =2.0; //错误,r-value为float 而 lvalue 为int.
int a =1.0+2;
float a =2;
float a =2.0+1;
bool a = 0; 
vec3 a = vec3(1.0, 2.0, 3.0) * 2;
```

## 精度限定

glsl在进行光栅化着色的时候,会产生大量的浮点数运算,这些运算可能是当前设备所不能承受的,所以glsl提供了3种浮点数精度,我们可以根据不同的设备来使用合适的精度.

在变量前面加上 `highp` `mediump` `lowp` 即可完成对该变量的精度声明.

```c
lowp float color;
varying mediump vec2 Coord;
lowp ivec2 foo(lowp mat3);
highp mat4 m;
```

我们一般在片元着色器(fragment shader)最开始的地方加上 `precision mediump float;` 便设定了默认的精度.这样所有没有显式表明精度的变量 都会按照设定好的默认精度来处理.

**如何确定精度:**

变量的精度首先是由精度限定符决定的,如果没有精度限定符,则要寻找其右侧表达式中,已经确定精度的变量,一旦找到,那么整个表达式都将在该精度下运行.如果找到多个, 则选择精度较高的那种,如果一个都找不到,则使用默认或更大的精度类型.

```c
uniform highp float h1;
highp float h2 = 2.3 * 4.7; //运算过程和结果都 是高精度
mediump float m;
m = 3.7 * h1 * h2; //运算过程 是高精度
h2 = m * h1; //运算过程 是高精度
m = h2 – h1; //运算过程 是高精度
h2 = m + m; //运算过程和结果都 是中等精度
void f(highp float p); // 形参 p 是高精度
f(3.3); //传入的 3.3是高精度
```

**`invariant`关键字:**

由于shader在编译时会进行一些内部优化,可能会导致同样的运算在不同shader里结果不一定精确相等.这会引起一些问题,尤其是vertx shader向fragmeng shader传值的时候. 所以我们需要使用 `invariant` 关键字来显式要求计算结果必须精确一致. 当然我们也可使用 `#pragma STDGL invariant(all)`来命令所有输出变量必须精确一致, 但这样会限制编译器优化程度,降低性能.

```c
#pragma STDGL invariant(all) //所有输出变量为 invariant
invariant varying texCoord; //varying在传递数据的时候声明为invariant
```

## 内置变量

顶点着色器可用的内置变量如下表：

| 名称                   | 类型  | 描述                                                                                      |
| ---------------------- | ----- | ----------------------------------------------------------------------------------------- |
| gl_Color               | vec4  | 输入属性-表示顶点的主颜色                                                                 |
| gl_SecondaryColor      | vec4  | 输入属性-表示顶点的辅助颜色                                                               |
| gl_Normal              | vec3  | 输入属性-表示顶点的法线值                                                                 |
| gl_Vertex              | vec4  | 输入属性-表示物体空间的顶点位置                                                           |
| gl_MultiTexCoordn      | vec4  | 输入属性-表示顶点的第n个纹理的坐标                                                        |
| gl_FogCoord            | float | 输入属性-表示顶点的雾坐标                                                                 |
| gl_Position            | vec4  | 输出属性-变换后的顶点的位置，用于后面的固定的裁剪等操作。所有的顶点着色器都必须写这个值。 |
| gl_ClipVertex          | vec4  | 输出坐标，用于用户裁剪平面的裁剪                                                          |
| gl_PointSize           | float | 点的大小                                                                                  |
| gl_FrontColor          | vec4  | 正面的主颜色的varying输出                                                                 |
| gl_BackColor           | vec4  | 背面主颜色的varying输出                                                                   |
| gl_FrontSecondaryColor | vec4  | 正面的辅助颜色的varying输出                                                               |
| gl_BackSecondaryColor  | vec4  | 背面的辅助颜色的varying输出                                                               |
| gl_TexCoord[]          | vec4  | 纹理坐标的数组varying输出                                                                 |
| gl_FogFragCoord        | float | 雾坐标的varying输出                                                                       |

片元着色器的内置变量如下表：

| 名称              | 类型  | 描述                                                                                     |
| ----------------- | ----- | ---------------------------------------------------------------------------------------- |
| gl_Color          | vec4  | 包含主颜色的插值只读输入                                                                 |
| gl_SecondaryColor | vec4  | 包含辅助颜色的插值只读输入                                                               |
| gl_TexCoord[]     | vec4  | 包含纹理坐标数组的插值只读输入                                                           |
| gl_FogFragCoord   | float | 包含雾坐标的插值只读输入                                                                 |
| gl_FragCoord      | vec4  | 只读输入，窗口的x,y,z和1/w                                                               |
| gl_FrontFacing    | bool  | 只读输入，如果是窗口正面图元的一部分，则这个值为true                                     |
| gl_PointCoord     | vec2  | 点精灵的二维空间坐标范围在(0.0, 0.0)到(1.0, 1.0)之间，仅用于点图元和点精灵开启的情况下。 |
| gl_FragData[]     | vec4  | 使用glDrawBuffers输出的数据数组。不能与gl_FragColor结合使用。                            |
| gl_FragColor      | vec4  | 输出的颜色用于随后的像素操作                                                             |
| gl_FragDepth      | float | 输出的深度用于随后的像素操作，如果这个值没有被写，则使用固定功能管线的深度值代替         |

## 修饰符

变量的声明可以使用如下的修饰符：

| 修饰符           | 描述                                                                                                                                                                                                                                                                                                                                |
| ---------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| const            | 常量值必须在声明时初始化。它是只读的不可修改的。                                                                                                                                                                                                                                                                                    |
| attribute        | 表示只读的顶点数据，只用在顶点着色器中。<br />数据来自当前的顶点状态或者顶点数组。它必须是全局范围声明的，不能在函数内部。<br />一个attribute可以是浮点数类型的标量，向量，或者矩阵。不可以是数组或者结构体                                                                                                                         |
| uniform          | 一致变量。在着色器执行期间一致变量的值是不变的。与const常量不同的是，<br />这个值在编译时期是未知的是由着色器外部初始化的。<br />一致变量在顶点着色器和片段着色器之间是共享的。它也只能在全局范围进行声明。                                                                                                                         |
| varying          | 顶点着色器的输出。例如颜色或者纹理坐标，（插值后的数据）作为片段着色器的只读输入数据。<br />必须是全局范围声明的全局变量。可以是浮点数类型的标量，向量，矩阵。不能是数组或者结构体。                                                                                                                                                |
| centorid varying | 在没有多重采样的情况下，与varying是一样的意思。在多重采样时，<br />centorid varying在光栅化的图形内部进行求值而不是在片段中心的固定位置求值。                                                                                                                                                                                       |
| invariant        | (不变量)用于表示顶点着色器的输出和任何匹配片段着色器的输入，<br />在不同的着色器中计算产生的值必须是一致的。所有的数据流和控制流，<br />写入一个invariant变量的是一致的。编译器为了保证结果是完全一致的，<br />需要放弃那些可能会导致不一致值的潜在的优化。除非必要，不要使用这个修饰符。在多通道渲染中避免z-fighting可能会使用到。 |
| in               | 用在函数的参数中，表示这个参数是输入的，在函数中改变这个值，<br />并不会影响对调用的函数产生副作用。（相当于C语言的传值），这个是函数参数默认的修饰符                                                                                                                                                                               |
| out              | 用在函数的参数中，表示该参数是输出参数，值是会改变的。                                                                                                                                                                                                                                                                              |
| inout            | 用在函数的参数，表示这个参数即是输入参数也是输出参数。                                                                                                                                                                                                                                                                              |

## **限定符的顺序**

当需要用到多个限定符的时候要遵循以下顺序:

1.在一般变量中: invariant > storage > precision

2.在参数中: storage > parameter > precision

我们来举例说明:

```none
invariant varying lowp float color; // invariant > storage > precision

void doubleSize(const in lowp float s){ //storage > parameter > precision
    float s1=s;
}
```

## 结构体‍

结构体的定义方式和C语言类似，可以组合基本类型和数组来形成用户自定义的类型, 区别是GLSL的结构体不支持嵌套定义，只有预先声明的结构体可以嵌套其中，参考代码如下：

```c
struct fogStruct
{
 vec4 color;
 float start;
 float end;
 vec3 points[3]; // 固定大小的数组是合法的
} fogVar;
```

```c
struct positionInfo 
{  
	vec2 coord;  
	float value;  
	rotateMatrix matrix;
}
```

可以通过 **=** 为结构体赋值，或者使用 **==，!=** 来判断两个结构体是否相等。

```c
fogVar = fogStruct(vec4(1.0,0.0,0.0,1.0),0.5,2.0);
vec4 color = fogVar.color;
float start = fogVar.start;
```

## 数组

**`GLSL`** 中只可以使用一维的数组。数组的类型可以是一切基本类型或者结构体。下面的几种数组声明是合法的：

```none
float floatArray[4];
vec4 vecArray[2];
float a[4] = float[](1.0,2.0,3.0,4.0);
vec2 c[2] = vec2[2](vec2(1.0,2.0),vec2(3.0,4.0));
```

数组类型内建了一个 `length()`函数，可以返回数组的长度。

```none
lightPositions.length() // 返回数组的长度
```

# GLSL表达式

## 操作符

GLSL语言的操作符与C语言相似。如下表（操作符的优先级从高到低排列）

| 操作符         | 描述                                                 |
| -------------- | ---------------------------------------------------- |
| ()             | 用于表达式组合，函数调用，构造                       |
| []             | 数组下标，向量或矩阵的选择器                         |
| .              | 结构体和向量的成员选择                               |
| ++ --          | 前缀或后缀的自增自减操作符                           |
| + – !         | 一元操作符，表示正 负 逻辑非                         |
| * /            | 乘 除操作符                                          |
| + -            | 二元操作符 表示加 减操作                             |
| <> <= >= == != | 小于，大于，小于等于， 大于等于，等于，不等于 判断符 |
| &&             | 逻辑与                                               |
| \|\|           | 逻辑或                                               |
| ^^             | 逻辑异或                                             |
| ?:             | 三元操作符                                           |
| = += –= *= /= | 赋值操作符                                           |
| ,              | 表示序列                                             |

求地址的& 和 解引用的 * 操作符不再GLSL中出现，因为GLSL不能直接操作地址。类型转换操作也是不允许的。

位操作符(&,|,^,~, <<, >> ,&=, |=, ^=, <<=, >>=)是GLSL保留的操作符，将来可能会被使用。还有求模操作（%，%=)也是保留的。

## **运算符**

| 优先级(越小越高) | 运算符        | 说明                                                                                           | 结合性 |
| ---------------- | ------------- | ---------------------------------------------------------------------------------------------- | ------ |
| 1                | ()            | 聚组:a*(b+c)                                                                                   | N/A    |
| 2                | [] () . ++ -- | 数组下标__[],方法参数__fun(arg1,arg2,arg3),属性访问**a.b** ,自增/减后缀**a++ a--** | L - R  |
| 3                | ++ -- + - !   | 自增/减前缀**++a --a** ,正负号(一般正号不写)a ,-a,取反**!false**                               | R - L  |
| 4                | * /           | 乘除数学运算                                                                                   | L - R  |
| 5                | + -           | 加减数学运算                                                                                   | L - R  |
| 7                | < > <= >=     | 关系运算符                                                                                     | L - R  |
| 8                | == !=         | 相等性运算符                                                                                   | L - R  |
| 12               | &&            | 逻辑与                                                                                         | L - R  |
| 13               | ^^            | 逻辑排他或(用处基本等于!=)                                                                     | L - R  |
| 14               | II            | 逻辑或                                                                                         | L - R  |
| 15               | **? :** | 三目运算符                                                                                     | L - R  |
| 16               | = += -= *= /= | 赋值与复合赋值                                                                                 | L - R  |
| 17               | ,             | 顺序分配运算                                                                                   | L - R  |

## 数组访问

和C语言一样， 数组的下标从0开始。取值范围是[0, sizeOfArray - 1]。如果越界了，会提示编译失败。

```c
vec4 myColor, ambient, diffuse[6], specular[6];
myColor = ambient + diffuse[4] + specular[4];
```

# GLSL语句

## **流控制**

GLSL的流控制和c语言非常相似,这里不必再做过多说明，唯一不同的是片元着色器中有一种特殊的控制流 `discard`. 使用discard会退出片元着色器，不执行后面的片段着色操作。片段也不会写入帧缓冲区。

```cpp
for (l = 0; l < numLights; l++)
{
    if (!lightExists[l]);
        continue;
    color += light[l];
}

...while (i < num)
{
    sum += color[i];
    i++;
}

...do{
    color += light[lightNum];
    lightNum--;
}while (lightNum > 0)...

//使用discard会退出片段着色器，不执行后面的片段着色操作。片元也不会写入帧缓冲区。
if (true)
    discard;
```

# GLSL函数

## 构造函数

glsl中变量可以在声明的时候初始化,`float pSize = 10.0` 也可以先声明然后等需要的时候在进行赋值.

聚合类型对象如(向量,矩阵,数组,结构) 需要使用其构造函数来进行初始化. `vec4 color = vec4(0.0, 1.0, 0.0, 1.0);`

```c
//一般类型
float pSize = 10.0;
float pSize1;
pSize1=10.0;
...

//复合类型
vec4 color = vec4(0.0, 1.0, 0.0, 1.0);
vec4 color1;
color1 =vec4(0.0, 1.0, 0.0, 1.0);
...

//结构
struct light {
    float intensity;
    vec3 position;
};
light lightVar = light(3.0, vec3(1.0, 2.0, 3.0));

//数组
const float c[3] = float[3](5.0, 7.2, 1.1);
```

## 自定义函数

GLSL允许在程序的最外部声明函数，函数不能嵌套，不能递归调用，且必须声明返回值类型(无返回值时声明为void) 在其他方面GLSL函数与C函数非常类似。参数的修饰符(in, out, inout, const等）是可选的。

每个shader中必须有一个main函数。

```cpp
vec4 getPosition(){
    vec4 v4 = vec4(0.,0.,0.,1.);
    return v4;
}

void doubleSize(inout float size){
    size= size*2.0  ;
}

void main() {
    float psize= 10.0;
    doubleSize(psize);
    gl_Position = getPosition();
    gl_PointSize = psize;
}
```

## 内置函数

**角和三角函数**

| 语法                                | 说明                             |
| ----------------------------------- | -------------------------------- |
| genType radians (genType degrees)   | 角度转弧度（degrees to radians） |
| genType degrees (genType radians)   | 弧度转角度（radians to degrees） |
| genType sin (genType angle)         | 三角函数-正弦sine                |
| genType cos (genType angle)         | 三角函数-余弦cosine              |
| genType tan (genType angle)         | 三角函数-正切tangent             |
| genType asin (genType x)            | 反三角函数-反正弦arc sine        |
| genType atan (genType y, genType x) | 反三角函数-反余弦arc cosine      |
| genType atan (genType y_over_x)     | 反三角函数-反正切arc tangent     |

**指数函数**

| 语法                               | 说明                                                                                    |
| ---------------------------------- | --------------------------------------------------------------------------------------- |
| genType pow (genType x, genType y) | x的y次方，\(x^y\)。如果x<0，则结果是undefined；<br />如果x=0并且y<=0，则结果是undefined |
| genType exp (genType x)            | x的自然指数，\(e^x\)                                                                    |
| genType log (genType x)            | x的自然对数，\(\log_ex\)，即\(\ln{x}\)x<=0时结果是undefined                             |
| genType exp2 (genType x)           | 2的x次方，\(2^x\)                                                                       |
| genType log2 (genType x)           | 以2为底，x的自然对数，\(log_2x\)，x<=0时结果是undefined                                 |
| genType sqrt (genType x)           | 对x进行开根号，\(\sqrt{x}\)，x<0时结果是undefined                                       |
| genType inversesqrt (genType x)    | sqrt的倒数，\(\frac{1}{\sqrt   x}\)，x<=0时结果是undefined                              |

**常用函数**

| 语法                                                        | 说明                                                                      |
| ----------------------------------------------------------- | ------------------------------------------------------------------------- |
| genType abs (genType x)                                     | x的绝对值                                                                 |
| genType sign (genType x)                                    | 判断x是正数、负数，还是零                                                 |
| genType floor (genType x)                                   | 返回不大于x的最大整数                                                     |
| genType ceil (genType x)                                    | 返回不小于x的最小整数                                                     |
| genType fract (genType x)                                   | 返回x的小数部分，即x-floor(x)                                             |
| genType mod (genType x, genType y)                          | 返回x – y * floor (x/y)                                                  |
| genType min (genType x, genType y)                          | 返回x和y的较小值                                                          |
| genType max (genType x, genType y)                          | 返回x和y的较大值                                                          |
| genType clamp (genType x, genType minVal,   genType maxVal) | min (max (x, minVal), maxVal)，<br />如果minVal > maxVal，则返回undefined |
| genType mix (genType x, genType y, genType   a)             | 返回x * (1−a) + y *   a                                                  |

几何函数

| 语法                                                      | 说明                                                                                                |
| --------------------------------------------------------- | --------------------------------------------------------------------------------------------------- |
| genType length (genType x)                                | 计算向量的长度，\(\sqrt{x1^2+x2^2+...}\)                                                            |
| genType distance (genType p0, genType p1)                 | p0和p1之间的距离，即length(p0-p1)                                                                   |
| genType dot (genType x, genType y)                        | 向量x与向量y的点积                                                                                  |
| genType cross (vec3 x, vec3 y)                            | 向量x与向量y的叉积                                                                                  |
| genType normalize (genType x)                             | 返回向量x对应的单位向量，即方向相同，长度为1                                                        |
| genType faceforward(genType N, genType I,   genType Nref) | 如果dot(Nref, I) < 0，则返回N，否则返回-N                                                           |
| genType reflect (genType I, genType N)                    | I是入射光的方向，N是反射平面的法线，返回值是反射光的方向。I – 2 * dot(N, I) * N。N必须是单位向量。 |
| genType refract(genType I, genType N, float   eta)        | I是入射光的方向，N是反射平面的法线，折射率是eta，返回值是折射后的光线的向量。I和N必须是单位向量。   |

矩阵函数

| 语法                              | 说明                                                                                                                   |
| --------------------------------- | ---------------------------------------------------------------------------------------------------------------------- |
| mat matrixCompMult (mat x, mat y) | 返回矩阵x乘以矩阵y的结果。例如result[i][j] 等于 x[i][j] * y[i][j]。注意：如果想进行线性代数里的乘法，请使用符号“*”。 |

向量关系函数

| 语法                                                                                      | 说明                      |
| ----------------------------------------------------------------------------------------- | ------------------------- |
| bvec lessThan(vec x, vec y) bvec lessThan(ivec   x, ivec y)                               | 判断x<y                   |
| bvec lessThanEqual(vec x, vec y) bvec   lessThanEqual(ivec x, ivec y)                     | 判断x<=y                  |
| bvec greaterThan(vec x, vec y) bvec   greaterThan(ivec x, ivec y)                         | 判断x>y                   |
| bvec greaterThanEqual(vec x, vec y) bvec   greaterThanEqual(ivec x, ivec y)               | 判断x>=y                  |
| bvec equal(vec x, vec y) bvec equal(ivec x,   ivec y) bvec equal(bvec x, bvec y)          | 判断x==y                  |
| bvec notEqual(vec x, vec y) bvec   notEqual(ivec x, ivec y) bvec notEqual(bvec x, bvec y) | 判断x!=y                  |
| bool any(bvec x)                                                                          | 判断x的元素是否有true     |
| bool all(bvec x)                                                                          | 判断x的元素是否全部为true |
| bool not(bvec x)                                                                          | `<!--TOTO-->`           |

Texture查找函数

| 函数                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | 说明                                                                                                                                                                                                             |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| vec4 texture2D   (sampler2D sampler, vec2 coord)<br />vec4 texture2D(sampler2D sampler, vec2 coord,   float bias)<br /> vec4 texture2DProj(sampler2D sampler, vec3 coord)<br /> vec4 texture2DProj (sampler2D sampler,vec3 coord, float bias) <br />vec4 texture2DProj   (sampler2D sampler, vec4coord) <br />vec4 texture2DProj (sampler2D sampler, vec4   coord, float bias)<br />vec4 texture2DLod (sampler2D sampler, vec2 coord, float   lod) <br />vec4texture2DProjLod (sampler2D sampler, vec3 coord, float lod) <br />vec4texture2DProjLod (sampler2D sampler, vec4 coord, float lod) | 使用texture坐标coord来查找当前绑定到采样器的texture。<br />对于函数名中含有Proj的函数来说，texture的坐标（coord.s,coord.t）<br />会先除以coord的最后一个坐标。对于vec4这种变种来说，坐标的第三个元素直接被忽略。 |
| vec4 textureCube   (samplerCube sampler, vec3 coord)<br />vec4 textureCube(samplerCube sampler, vec3   coord, float bias)<br />vec4 textureCubeLod(samplerCube sampler, vec3 coord, float   lod)                                                                                                                                                                                                                                                                                                                                                                                               | 使用coord这个坐标去查找当前绑定到采样器的cube map。<br />coord的方向用来表示去查找cube map的哪一个二维平面。<br />OpenGL说明书的2.0版本的3.8.6小节详细描述了这一点。                                             |

# GLSL预编译指令

以 # 开头的是预编译指令,常用的有:

```cpp
#define #undef #if #ifdef #ifndef #else
#elif #endif #error #pragma #extension #version #line
```

比如 **#version 100** 他的意思是规定当前shader使用 GLSL ES 1.00标准进行编译,如果使用这条预编译指令,则他必须出现在程序的最开始位置.

**内置的宏:**

`__LINE__` : 当前源码中的行号.

`__VERSION__` : 一个整数,指示当前的glsl版本 比如 100 ps: 100 = v1.00

`GL_ES` : 如果当前是在 OPGL ES 环境中运行则 GL_ES 被设置成1,一般用来检查当前环境是不是 OPENGL ES.

`GL_FRAGMENT_PRECISION_HIGH` : 如果当前系统glsl的片元着色器支持高浮点精度,则设置为1.一般用于检查着色器精度.

实例:

1.如何通过判断系统环境,来选择合适的精度:

```none
#ifdef GL_ES //
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif
```

2.自定义宏:

```none
#define NUM 100
#if NUM==100
#endif
```
