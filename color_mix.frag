#ifdef GL_ES
precision mediump float;//中等精度的浮点
#endif

#define PI 3.141592653589793

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

vec3 colorA=vec3(.149,.141,.912);
vec3 colorB=vec3(1.,.833,.224);

float plot(vec2 st,float pct)
{
    return smoothstep(pct-.01,pct,st.y)-
    smoothstep(pct,pct+.01,st.y);
}

vec3 simple_mix()
{
    
    vec3 color=vec3(0.);
    
    float pct=abs(sin(u_time));
    
    // Mix uses pct (a value from 0-1) to
    // mix the two colors
    color=mix(colorA,colorB,pct);
    // color=vec3(plot(st,st.x));
    
    return color;
}
vec3 complex_mix()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    vec3 color=vec3(0.);
    
    vec3 pct=vec3(st.x);
    
    // pct.r = smoothstep(0.0,1.0, st.x);
    // pct.g = sin(st.x*PI);
    // pct.b = pow(st.x,0.5);
    
    color=mix(colorA,colorB,pct);
    
    // Plot transition lines for each channel
    color=mix(color,vec3(1.,0.,0.),plot(st,pct.r));
    color=mix(color,vec3(0.,1.,0.),plot(st,pct.g));
    color=mix(color,vec3(0.,0.,1.),plot(st,pct.b));
    // color = vec3(plot(st,st.x));
    return color;
}

vec3 sun_rise()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy-.5;
    vec3 color=vec3(0.);
    st += vec2(cos(u_time),sin(u_time))*0.5;
    
    color=vec3(1.-step(.5,length(st)))*mix(vec3(1.,1.,0.),vec3(0.,1.,1.),st.y);
    return color;
}
vec3 flag()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    st=st*2.;
    vec3 color=vec3(0.);
    vec2 b=step(vec2(.1),st);
    //转置坐标系
    vec2 r=step(vec2(0.),1.-st);
    vec2 b2=step(vec2(1.),st);
    vec2 r2=step(vec2(.1),2.-st);
    color=vec3(r2.x*r2.y*b2.x*b2.y+r.x*r.y*b.x*b.y)*vec3(1.,0.,0.);
    color+=vec3(step(.1,1.-st.x)*step(1.1,st.y)*step(.1,st.x)*step(.1,2.-st.y));
    
    // color = vec3(r2.x * r2.y);
    // color =vec3(b.x * b.y * r.x * r.y );
    return color;
}
vec3 flag2()
{
    vec2 st=gl_FragCoord.xy/u_resolution.xy;
    vec3 color=vec3(0.);
    float x1=step(.1,st.x);
    float x2=step(.2,1.-st.x);
    color=vec3(x1*x2)*vec3(1.,0.,0.);
    // color+=vec3(x1*x2)+vec3(1.,0.,0.);
    return color;
}

void main()
{
    vec3 color=simple_mix();
    // color=complex_mix();
    // color=sun_rise();
    // color=flag();
    // color=flag2();
    gl_FragColor=vec4(color,1.);
}
/*
y = fract(sin(x)*1.0);
值域为-1.0 到 1.0 之间的sin() 函数被取了小数点后的部分(这里实际是指模1))，返回0.0 到 1.0 间的正值。我们可以用这种效果通过把正弦函数打散成小片段来得到一些伪随机数。如何实现呢？通过在sin(x)的值上乘以大些的数。
*/