#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

#if 1
float line_width = 0.01;
#else
//让线宽随时间变化
float line_width = 0.01 * (sin(u_time)+1.0);
#endif

float plot(vec2 st) 
{    
    return 1.-smoothstep(0.00, line_width, abs(st.y - st.x));
}

//对比一下step版本的plot
float step_plot(vec2 st)
{
    return 1.-step(line_width,abs(st.y - st.x));
}

void main() 
{
    //归一化
	vec2 st = gl_FragCoord.xy/u_resolution;

    float y = st.x;

    vec3 color = vec3(y);

    //在这条线上的的值为1.0，其余地方为0.0
    float pct = plot(st);
    pct = step_plot(st);
    //1.0- pct 相当于取反
    color = (1.0-pct)*color+pct*vec3(0.0,1.0,0.0);

	gl_FragColor = vec4(color,1.0);
}