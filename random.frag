#ifdef GL_ES
//浮点精度
precision mediump float;
#endif

#define PI 3.14159265359
#define TWO_PI 6.28318530718

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

//二维随机数
float random(vec2 st)
{
    return fract(sin(dot(st.xy,vec2(12.9898,78.233)))*43758.5453123);
}


// 2D Noise based on Morgan McGuire @morgan3d
// https://www.shadertoy.com/view/4dS3Wd
float noise(in vec2 st)
{
    vec2 i=floor(st);
    vec2 f=fract(st);
    
    // Four corners in 2D of a tile
    float a=random(i);
    float b=random(i+vec2(1.,0.));
    float c=random(i+vec2(0.,1.));
    float d=random(i+vec2(1.,1.));
    
    // Smooth Interpolation
    
    // Cubic Hermine Curve.  Same as SmoothStep()
    vec2 u=f*f*(3.-2.*f);
    // u = smoothstep(0.,1.,f);
    
    // Mix 4 coorners percentages
    return mix(a,b,u.x)+
    (c-a)*u.y*(1.-u.x)+
    (d-b)*u.x*u.y;
}

float circle(vec2 st,vec2 center,float radius)
{
    return 1.-step(radius,length(st-center));
}
mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
    sin(_angle),cos(_angle));
}
void main()
{
    vec2 st=gl_FragCoord.xy/u_resolution;
    st*=10.;
    vec2 ipos=floor(st);
    vec2 fpos=fract(st);
    
    vec3 color=vec3(0.);
    
    // color = vec3(fract(sin(st.x)*10000.0));
    //看起来像电视的信号
    color=vec3(random(st*u_time));
    //闪烁的格子
    color=vec3(random(ipos*u_time));
    //在10*10的格子里画随机半径随机颜色的圆
    color=vec3(0.);
    // vec2 translate=vec2(cos(u_time*random(ipos)),sin(u_time*random(ipos)));
    // st+=translate;
    for(int x=0;x<10;x++)
    {
        for(int y=0;y<10;y++)
        {
            color+=vec3(circle(st,vec2(float(x)+.5,float(y)+.5),noise(st*(cos(noise(st)*u_time)+2.0)/2.)))
            *vec3(noise(st*(cos(u_time)+1.0)/2.),noise(st*(sin(u_time)+1.0)/2.),noise(st*(sin(u_time)*cos(u_time)+1.0)/2.));
        }
    }
    // //使用噪声
    // color=vec3(0.);
    // color+=smoothstep(.15,.2,noise(st));
    // color-=smoothstep(.35,.4,noise(st));
    
    gl_FragColor=vec4(color,1.);
}
