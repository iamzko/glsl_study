#ifdef GL_ES
//浮点精度
precision mediump float;
#endif

#define PI 3.14159265359

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

vec3 colorA = vec3(0.149,0.141,0.912);
vec3 colorB = vec3(1.000,0.833,0.224);

void main() {
    vec3 color = vec3(0.0);

    float pct = abs(sin(u_time));

 //mix混合颜色 colorA * pct + colorB * (1.0 - pct)
    color = mix(colorA, colorB, pct);

    gl_FragColor = vec4(color,1.0);
}